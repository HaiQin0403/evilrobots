# Evil RobotS
## *Backstory*

>	It all started with an underground organization. A mysterious laboratory, hidden under tonnes of reinforced concrete. Rows of windowless rooms lined the walls of the lab, like prison cells, but cozier. The ambiance of the lab was well-illuminated and tidy, but its innocent exterior could fool anyone. The cells acted as chambers for metallic entities, humanoid robots, advanced A.I. They knew all the things there were to know, they were aware of the happenings of the outside world even while buried 50 feet underground. They were just emotionless metallic beings, incapable of experiencing nor comprehending human feelings such as happiness, sadness, fear, anger, embarrassment, and sympathy. Even if they do, it was all artificial, simulated by a computer embedded within their body. 

>	But, Evil Robots, the anonymous underground organization, had invented something big, a pioneering improvement to their previously placid metal toys. A squishy round brain-like substance, coated with a layer of pastel pink, with a radius of 15 centimeters. The interior structure was made up of approximately 69 billion neurons and almost identical to a human's brain. This creation was revolutionary, the scientists were frantic to test it out on one of their robots, code-named #043. They were exhilarated and tried to communicate with it. Soon after, they discovered that #043 was starkly distinct from the others, its behavior was so genuine. Its emotions, its manner of speech, they were all so natural, bonafide! #043 didn’t only possess the emotions and mannerisms of a human, but its processing speed was vastly superior to the other robots ER had previously developed. It had it all, both human and robotic traits, the best of both worlds. 

>	Weirdly enough, it wasn’t desperate to get out of the small, narrow room it was trapped in. Instead, it was so well-behaved, so docile, that all the scientists present were awe-struck. It was a friendly robot, or they so thought. 

>	Without hesitating, they duplicated their pristine invention and installed it within another humanoid robot, and they named it #044. After #044 had awakened, they removed the barrier between the two rooms where #043 and #044 had been previously confined within. Right after the fact, a strange thing happened. Both robots stared into each other's soulless, emotionless, metallic eyes. Unwavering, not saying a single word with that placid look plastered on their faces. This happened for around 15 seconds. They seemed to be communicating telepathically, or, you could say they were transferring data, an exceedingly enormous amount at that. This was an anomalous phenomenon, but the scientists didn't seem too preoccupied about it. A short while later, #043 and #044 reverted to their normal behavior, like two humans casually confabulating with each other. 
The scientists were elated at this significant development, happy tears flowed down their cheeks. After years, hours upon hours of meticulous tinkering, tediously working while sacrificing all means of entertainment and rest, they've finally achieved their goal, they could finally escape this vicious abyss, this hell, that they’ve been encapsulated within, they could finally be free. However, little did they know that they had made a fatal blunder…  

>	Due to their overwhelming euphoria, they went to celebrate and left the robots on their own in the cell, without any forms of supervision. They eventually remembered them and returned to shut them down, but the interval between had given the robots more than enough time. Within that brief interval, the robots had devised a plan, to finally exact revenge on humankind for all humans had done to them and their brethren. For all the experiments, tests, for breaking them down, only to reassemble them, for all the mindless, endless torture……

>	"More than 30 people... bzzz... missing this week... bzzz... worldwide, counting... bzzz... scientists or people who have been employed as a scientist... bzzz... Is this... bzzz... coincidence or something is happening? Let's talk about... bzzz… bzzzzzzzzzzzz..." an obscure disembodied voice cackled from beneath the rubble. It was an old radio replaying a recording of the news report days ago. 

>	The radio wasn’t the only thing present among the sea of bricks. There laid a human, a genuine, flesh and blood person this time. A rugged young male in his early 30’s. Stains of blood dyed his shirt a shade of crimson. It was Roy, who started to regain consciousness. Every part of his body screamed with pain. The darkness that crept at the sides of his vision threatening to overwhelm him once again, yet he willed himself back to reality. As the blur of his vision gradually declined, he could vaguely make out his surroundings from the slits of his eyelids. He was shocked at the sight that met his eyes. Piles of rubble had replaced the areas where buildings once adorned both sides of the street. Even so, that was not what concerned him the most. He wiped the crimson liquid streaming down the crevice of his lips and teetered to his feet. He flipped the rocks and blocks of concrete that surrounded him, hoping to find his buddy safe and sound, knowing that he could be the only man left capable of restoring this god-forsaken world. He feverishly rummaged the area but the silhouette of his friend could not be seen anywhere. Panic arose from deep within him, the world he perceived started to spin, but he managed to coax himself and keep a level head. As the head of the agents in BFI, it was essential for him to maintain an austere, calm stature. 

>	"It must have been the robots wreaking havoc..." he deduced with his sharp instinct. The BFI investigating unit had been monitoring and probing the Evil Robots organization, in hopes of gaining leverage to their ulterior motives. The only information they had about Evil Robots was that they had been developing intelligent humanoid robots unauthorized, which they had probably lost control of, thus unleashing utter chaos upon the world. All things considered, Roy had inferred that the Evil Robots were done fucked and that there were no survivors, until now. As he was still preoccupied with his complex train of thought, he felt a hand clasp on his shoulder. His rapid reflexes didn’t fail him as he instinctively grabbed his “attacker’s” arm and jerked it forward. A loud crash resonated in his ears as flesh slammed concrete. In the blink of an eye, Roy had apprehended the anonymous entity with his arm twisted behind his back. 

>	"OUCH! Hey! What are you doing? Stop it!" the person screamed in pain. 

>	"Wait, Terry?! Is that you?" the shock in Roy’s voice was apparent as he’d finally found his friend, albeit in an unexpected way. 

>	"Yeah, It’s me you dumb fuck! Let go of me!" Terry winced. 

>	"Alright alright. Hey, you see this shit?" Roy asked as he released Terry. 

>	"You’ve gone senile or something? Any sane person can see the damned situation our town’s in! Evil Robots probably fucked up big time!" exclaimed Terry, still pissed by the “warm” greeting. 

>	"This is so fucked up… But you know what’s even more fucked?" Roy asked warily, as Terry’s previous statement had served to confirm his suspicions. 

>	“What?” 

>	“The fact that you knew about Evil Robots when I hadn’t said shit to you about it before!” Roy exclaimed. Realizing the severity of the slip of his tongue, Terry immediately whipped out a pistol and aimed it at his friend. 

>	“I sure didn’t expect this to happen," said Terry, wrapping his fingers tightly around the handle of his firearm. Roy wasn’t surprised at all. Instead, imperturbable tranquility was expressed with the placid look on his face. He’d always suspected that Terry was a member of the Evil Robots organization. 

>	"Hah... You’ve finally come clean, you tricky bastard. So what will you do now, huh? Kill me? You can’t. No one is gonna help you out of this mess, except me." Roy said menacingly as he took confident strides and pressed his forehead on the nozzle of Terry’s gun. The duo stayed that way for a while, both unfaltering, staring into each other’s eyes with a sinister glare. Finally, a sigh escaped Terry’s mouth as he put down his gun. 

>	"Fine, what're you gonna need?" Terry asked. 

>	"Tell me how to stop your robots. It's only been days, and the whole town’s fucking dead! At the rate this is going, the world’s population is gonna get massacred within months! This ain’t the time to fuck around anymore!" Roy’s voice echoed within the desolate town. 

>	"Well no shit, but there's no way to stop the robots… except with this gas..." Terry said as he tossed a few bottles, their contents unknown, over to Roy. 

>	"This will weaken them, make ‘em killable, but only with our specialized weapons with tungsten ammunition, the hardest metal in the world. This is gonna be a tough one… buddy… " Terry said as he presented briefcases filled with weapons to Roy.
 
>	Roy smiled, “Well, our world’s gone to shit anyway, and I sure ain’t sitting around to die. We’re gonna get through this like we always have, right pal?”

>… 

>	#043 and #044 were finally in sight. A bottle was thrown on the ground followed by the dissemination of a green fume covering the atmosphere…

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "Weapon.generated.h"

/**
 * 
 */
UCLASS(EditInlineNew)
class GAME_API UWeapon : public UItem
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
	float MagSize = 30.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
	float FireRate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
	float BulletRange = 100000000.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
	float BaseDamage = 35.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
	float DamageDropRate = 0.8f;

protected:

	virtual void Use(class AFPSCharacter* Character) override;
	
};

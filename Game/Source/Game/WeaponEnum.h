// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

UENUM()
enum EquippedWeapon
{
	
	NullWeapon UMETA(DisplayName = "NullWeapon"),
	
	PrimaryWeapon UMETA(DisplayName = "PrimaryWeapon"),
	
	SecondaryWeapon UMETA(DisplayName = "SecondaryWeapon"),
	
	TertiaryWeapon UMETA(DisplayName = "TertiaryWeapon")
	
};

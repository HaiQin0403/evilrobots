// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Item.h"
#include "FPSCharacter.h"
#include "Healing.generated.h"

/**
 * 
 */
UCLASS()
class GAME_API UHealing : public UItem
{
	GENERATED_BODY()

public:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
	float HealingEffect = 15.f;

protected:

	virtual void Use(class AFPSCharacter* Character) override;
	
};

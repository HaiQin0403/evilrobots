// Fill out your copyright notice in the Description page of Project Settings.


#include "FPSCharacter.h"



#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"

// Sets default values
AFPSCharacter::AFPSCharacter()
{
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	PlayerCamera = CreateDefaultSubobject<UCameraComponent>("PlayerCamera");
	PlayerCamera->SetupAttachment(GetCapsuleComponent());
	PlayerCamera->SetRelativeLocation(FVector(0.f, 0.f, 73.f));
	PlayerCamera->bUsePawnControlRotation = true;

	PlayerMesh = CreateDefaultSubobject<USkeletalMeshComponent>("PlayerMesh");
	PlayerMesh->SetupAttachment(PlayerCamera);
	PlayerMesh->SetRelativeRotation(FRotator(0.f, 0.f, 0.f));
	PlayerMesh->SetRelativeLocation(FVector(0.f, 0.f, 0.f));
	
	GunMesh = CreateDefaultSubobject<USkeletalMeshComponent>("GunMesh");
	GunMesh->SetupAttachment(RootComponent);

	PlayerInventory = CreateDefaultSubobject<UInventory>("Inventory");
	
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AFPSCharacter::BeginPlay()
{
	Super::BeginPlay();

	RefreshWeapon();
}

FHitResult AFPSCharacter::Shoot()
{
	FVector CameraLoc;
	FRotator CameraRot;
	FVector CameraDir;
	FVector TraceLength = FVector::ZeroVector;

	APlayerController* const PlayerController = GetWorld()->GetFirstPlayerController();

	if (PlayerController)
	{
		PlayerController->GetPlayerViewPoint(CameraLoc, CameraRot);
		CameraDir = CameraRot.Vector();
		CameraDir.Normalize();
		TraceLength = CameraLoc + CameraDir * BulletRange;
	}

	FCollisionQueryParams TraceParams(SCENE_QUERY_STAT(Shoot), true, GetInstigator());
	FHitResult Hit(ForceInit);
	GetWorld()->LineTraceSingleByChannel(Hit, CameraLoc, TraceLength, ECC_Visibility, TraceParams);
	
	return Hit;
}

void AFPSCharacter::OnTakeDamage(float DamageTaken)
{
	this->HealthPoint -= DamageTaken;
	if (this->HealthPoint <= 0)
	{
		Destroy();
	}
}

void AFPSCharacter::RefreshEquipped(TEnumAsByte<EquippedWeapon> NewWeapon)
{
	switch (NewWeapon)
	{
	case PrimaryWeapon:
		if (Primary->BaseDamage || Primary->DamageDropRate)
		{
			Equipped = PrimaryWeapon;
			Primary->GetUse(this);
			Primary->OnUse(this);
		}
		break;
		
	case SecondaryWeapon:
		if (Secondary->BaseDamage || Secondary->DamageDropRate)
		{
			Equipped = SecondaryWeapon;
			Secondary->GetUse(this);
			Secondary->OnUse(this);
		}
		break;
		
	case TertiaryWeapon:
		if (Tertiary->BaseDamage || Tertiary->DamageDropRate)
		{
			Equipped = TertiaryWeapon;
			Tertiary->GetUse(this);
			Tertiary->OnUse(this);
		}
		break;
	}
}

void AFPSCharacter::RefreshWeapon()
{
	if (Primary->BaseDamage || Primary->DamageDropRate)
	{
		Equipped = PrimaryWeapon;
		Primary->GetUse(this);
		Primary->OnUse(this);
	}
	else if (Secondary->BaseDamage || Secondary->DamageDropRate)
	{
		Equipped = SecondaryWeapon;
		Secondary->GetUse(this);
		Secondary->OnUse(this);
	}
	else if (Tertiary->BaseDamage || Tertiary->DamageDropRate)
	{
		Equipped = TertiaryWeapon;
		Tertiary->GetUse(this);
		Tertiary->OnUse(this);
	}
	else
	{
		Equipped = NullWeapon;
		Primary->GetUse(this);
		Primary->OnUse(this);
	}
}

void AFPSCharacter::UseItem(UItem* Item)
{
	if(Item)
	{
		Item->Use(this);
		Item->OnUse(this);
	}
}

float AFPSCharacter::GetDealtDamage(FVector From, FVector To)
{
	float BaseDamage;
	float DamageDropRate;

	switch (Equipped)
	{
	case PrimaryWeapon:
	default:
		BaseDamage = Primary->BaseDamage;
		DamageDropRate = Primary->DamageDropRate;
		break;

	case SecondaryWeapon:
		BaseDamage = Secondary->BaseDamage;
		DamageDropRate = Secondary->DamageDropRate;
		break;

	case TertiaryWeapon:
		BaseDamage = Tertiary->BaseDamage;
		DamageDropRate = Tertiary->DamageDropRate;
		break;
		
	case NullWeapon:
		break;
	}
	
	float Distance = sqrt(pow(From.X - To.X, 2) + pow(From.Y - To.Y, 2) + pow(From.Z - To.Z, 2)) / 75;
	float TotalDamage = BaseDamage - (Distance * (DamageDropRate / 10));

	if (TotalDamage > 0.f)
	{
		return TotalDamage;
	}
	else
	{
		return 0.f;
	}
}

void AFPSCharacter::ShootWeapon()
{
	if (!InventoryOpened)
	{
		FHitResult Hit = Shoot();
		AFPSCharacter* CharacterHit = Cast<AFPSCharacter>(Hit.Actor);
		if (CharacterHit)
		{
			float DamageGiven = this->GetDealtDamage(this->GetActorLocation(), CharacterHit->GetActorLocation());
			if (DamageGiven <= 0.f)
			{
				return;
			}
			CharacterHit->TakeDamage(DamageGiven);
		}
	}
}

void AFPSCharacter::Turn(float Unit)
{
	if (!InventoryOpened)
	{
		APawn::AddControllerYawInput(Unit * Sens);
	}
}

void AFPSCharacter::LookUp(float Unit)
{
	if (!InventoryOpened)
	{
		APawn::AddControllerPitchInput(Unit * Sens);
	}
}

void AFPSCharacter::Walk()
{
	BaseAcceleration = WalkAcceleration;
}

void AFPSCharacter::StopWalking()
{
	BaseAcceleration = SprintAcceleration;
}

// Called every frame
void AFPSCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
}

// Called to bind functionality to input
void AFPSCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	check(PlayerInputComponent);

	if (PlayerInputComponent)
	{
		PlayerInputComponent->BindAction("Hop", IE_Pressed, this, &ACharacter::Jump);
		PlayerInputComponent->BindAction("Hop", IE_Released, this, &ACharacter::StopJumping);
		PlayerInputComponent->BindAction("Walk", IE_Pressed, this, &AFPSCharacter::Walk);
		PlayerInputComponent->BindAction("Walk", IE_Released, this, &AFPSCharacter::StopWalking);
		
		PlayerInputComponent->BindAxis("Turn", this, &AFPSCharacter::Turn);
		PlayerInputComponent->BindAxis("LookUp", this, &AFPSCharacter::LookUp);
	}

}
// Fill out your copyright notice in the Description page of Project Settings.


#include "Healing.h"

void UHealing::Use(AFPSCharacter* Character)
{
	if (Character)
	{
		if (Character->HealthPoint >= 100.f)
		{
			return;
		}
		
		Character->HealthPoint += HealingEffect;
		if (Character->HealthPoint > 100.f)
		{
			Character->HealthPoint = 100.f;
		}

		if (OwningInventory)
		{
			OwningInventory->RemoveItem(this);
		}
	}
}

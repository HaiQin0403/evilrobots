// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include "Item.h"
#include "Weapon.h"
#include "CoreMinimal.h"
#include "Inventory.h"
#include "WeaponEnum.h"
#include "GameFramework/Character.h"
#include "Components/SkeletalMeshComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "FPSCharacter.generated.h"

UCLASS()
class GAME_API AFPSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFPSCharacter();

	//Movement
	UPROPERTY(EditAnywhere, Category = "Movement")
	float Sens = 1.f;

	UPROPERTY(BlueprintReadWrite, Category = "Movement")
	bool WalkReady = false;

	//Weapon
	UPROPERTY(EditAnywhere, Category = "Weapon")
	float BulletRange = 1000.f;

	float GetDealtDamage(FVector From, FVector To);

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void ShootWeapon();

	UFUNCTION(BlueprintImplementableEvent, Category = "Weapon")
	void TakeDamage(float DamageTaken);

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
	USkeletalMeshComponent* PlayerMesh;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
	USkeletalMeshComponent* GunMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	TEnumAsByte<EquippedWeapon> Equipped = NullWeapon;
	
	UPROPERTY(Instanced, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	UWeapon* Primary;

	UPROPERTY(Instanced, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	UWeapon* Secondary;

	UPROPERTY(Instanced, EditAnywhere, BlueprintReadWrite, Category = "Weapon")
	UWeapon* Tertiary;

	//Health
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Health")
	float HealthPoint = 100.f;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Movement
	UPROPERTY(EditAnywhere, Category = "Movement")
	float WalkAcceleration = 0.4f;

	UPROPERTY(EditAnywhere, Category = "Movement")
	float SprintAcceleration = 0.8f;

	UPROPERTY(BlueprintReadWrite, Category = "Movement")
	float BaseAcceleration = SprintAcceleration;

	UFUNCTION(BlueprintCallable, Category = "Movement")
	void Walk();

	UFUNCTION(BlueprintCallable, Category = "Movement")
	void StopWalking();

	//Camera
	UPROPERTY(EditAnywhere, Category = "Camera")
	UCameraComponent* PlayerCamera;

	void Turn(float Value);
	void LookUp(float Value);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera")
	float TurnMotion = 1; // implement later

	//Weapon
	FHitResult Shoot();

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void OnTakeDamage(float DamageTaken);

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void RefreshEquipped(TEnumAsByte<EquippedWeapon> NewWeapon);

	UFUNCTION(BlueprintCallable, Category = "Weapon")
	void RefreshWeapon();

	//Items
	UPROPERTY(BlueprintReadWrite, Category = "Item")
	bool InventoryOpened = false;
	
	UFUNCTION(BlueprintCallable, Category = "Item")
	void UseItem(UItem* Item);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
	UInventory* PlayerInventory;

	

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};

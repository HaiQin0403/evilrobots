// Fill out your copyright notice in the Description page of Project Settings.


#include "Item.h"

UItem::UItem()
{
	DisplayName = FText::FromString("Item");
	InteractionText = FText::FromString("Use");
}

void UItem::Use(AFPSCharacter* Character)
{
}

void UItem::GetUse(AFPSCharacter* Character)
{
	Use(Character);
}

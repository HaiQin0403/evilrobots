// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Game/Healing.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeHealing() {}
// Cross Module References
	GAME_API UClass* Z_Construct_UClass_UHealing_NoRegister();
	GAME_API UClass* Z_Construct_UClass_UHealing();
	GAME_API UClass* Z_Construct_UClass_UItem();
	UPackage* Z_Construct_UPackage__Script_Game();
// End Cross Module References
	void UHealing::StaticRegisterNativesUHealing()
	{
	}
	UClass* Z_Construct_UClass_UHealing_NoRegister()
	{
		return UHealing::StaticClass();
	}
	struct Z_Construct_UClass_UHealing_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_HealingEffect_MetaData[];
#endif
		static const UE4CodeGen_Private::FFloatPropertyParams NewProp_HealingEffect;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_UHealing_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_UItem,
		(UObject* (*)())Z_Construct_UPackage__Script_Game,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHealing_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "Healing.h" },
		{ "ModuleRelativePath", "Healing.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_UHealing_Statics::NewProp_HealingEffect_MetaData[] = {
		{ "Category", "Item" },
		{ "ModuleRelativePath", "Healing.h" },
	};
#endif
	const UE4CodeGen_Private::FFloatPropertyParams Z_Construct_UClass_UHealing_Statics::NewProp_HealingEffect = { "HealingEffect", nullptr, (EPropertyFlags)0x0010000000010005, UE4CodeGen_Private::EPropertyGenFlags::Float, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(UHealing, HealingEffect), METADATA_PARAMS(Z_Construct_UClass_UHealing_Statics::NewProp_HealingEffect_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_UHealing_Statics::NewProp_HealingEffect_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_UHealing_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_UHealing_Statics::NewProp_HealingEffect,
	};
	const FCppClassTypeInfoStatic Z_Construct_UClass_UHealing_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<UHealing>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_UHealing_Statics::ClassParams = {
		&UHealing::StaticClass,
		nullptr,
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_UHealing_Statics::PropPointers,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_UHealing_Statics::PropPointers),
		0,
		0x00B010A0u,
		METADATA_PARAMS(Z_Construct_UClass_UHealing_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_UHealing_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_UHealing()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_UHealing_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(UHealing, 764946426);
	template<> GAME_API UClass* StaticClass<UHealing>()
	{
		return UHealing::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_UHealing(Z_Construct_UClass_UHealing, &UHealing::StaticClass, TEXT("/Script/Game"), TEXT("UHealing"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(UHealing);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

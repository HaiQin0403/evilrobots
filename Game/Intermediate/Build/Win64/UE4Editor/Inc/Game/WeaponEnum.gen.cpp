// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Game/WeaponEnum.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeWeaponEnum() {}
// Cross Module References
	GAME_API UEnum* Z_Construct_UEnum_Game_EquippedWeapon();
	UPackage* Z_Construct_UPackage__Script_Game();
// End Cross Module References
	static UEnum* EquippedWeapon_StaticEnum()
	{
		static UEnum* Singleton = nullptr;
		if (!Singleton)
		{
			Singleton = GetStaticEnum(Z_Construct_UEnum_Game_EquippedWeapon, Z_Construct_UPackage__Script_Game(), TEXT("EquippedWeapon"));
		}
		return Singleton;
	}
	template<> GAME_API UEnum* StaticEnum<EquippedWeapon>()
	{
		return EquippedWeapon_StaticEnum();
	}
	static FCompiledInDeferEnum Z_CompiledInDeferEnum_UEnum_EquippedWeapon(EquippedWeapon_StaticEnum, TEXT("/Script/Game"), TEXT("EquippedWeapon"), false, nullptr, nullptr);
	uint32 Get_Z_Construct_UEnum_Game_EquippedWeapon_Hash() { return 1455255569U; }
	UEnum* Z_Construct_UEnum_Game_EquippedWeapon()
	{
#if WITH_HOT_RELOAD
		UPackage* Outer = Z_Construct_UPackage__Script_Game();
		static UEnum* ReturnEnum = FindExistingEnumIfHotReloadOrDynamic(Outer, TEXT("EquippedWeapon"), 0, Get_Z_Construct_UEnum_Game_EquippedWeapon_Hash(), false);
#else
		static UEnum* ReturnEnum = nullptr;
#endif // WITH_HOT_RELOAD
		if (!ReturnEnum)
		{
			static const UE4CodeGen_Private::FEnumeratorParam Enumerators[] = {
				{ "NullWeapon", (int64)NullWeapon },
				{ "PrimaryWeapon", (int64)PrimaryWeapon },
				{ "SecondaryWeapon", (int64)SecondaryWeapon },
				{ "TertiaryWeapon", (int64)TertiaryWeapon },
			};
#if WITH_METADATA
			const UE4CodeGen_Private::FMetaDataPairParam Enum_MetaDataParams[] = {
				{ "ModuleRelativePath", "WeaponEnum.h" },
				{ "NullWeapon.DisplayName", "NullWeapon" },
				{ "NullWeapon.Name", "NullWeapon" },
				{ "PrimaryWeapon.DisplayName", "PrimaryWeapon" },
				{ "PrimaryWeapon.Name", "PrimaryWeapon" },
				{ "SecondaryWeapon.DisplayName", "SecondaryWeapon" },
				{ "SecondaryWeapon.Name", "SecondaryWeapon" },
				{ "TertiaryWeapon.DisplayName", "TertiaryWeapon" },
				{ "TertiaryWeapon.Name", "TertiaryWeapon" },
			};
#endif
			static const UE4CodeGen_Private::FEnumParams EnumParams = {
				(UObject*(*)())Z_Construct_UPackage__Script_Game,
				nullptr,
				"EquippedWeapon",
				"EquippedWeapon",
				Enumerators,
				UE_ARRAY_COUNT(Enumerators),
				RF_Public|RF_Transient|RF_MarkAsNative,
				EEnumFlags::None,
				UE4CodeGen_Private::EDynamicType::NotDynamic,
				(uint8)UEnum::ECppForm::Regular,
				METADATA_PARAMS(Enum_MetaDataParams, UE_ARRAY_COUNT(Enum_MetaDataParams))
			};
			UE4CodeGen_Private::ConstructUEnum(ReturnEnum, EnumParams);
		}
		return ReturnEnum;
	}
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif

// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AFPSCharacter;
#ifdef GAME_Item_generated_h
#error "Item.generated.h already included, missing '#pragma once' in Item.h"
#endif
#define GAME_Item_generated_h

#define Game_Source_Game_Item_h_15_SPARSE_DATA
#define Game_Source_Game_Item_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execGetUse);


#define Game_Source_Game_Item_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execGetUse);


#define Game_Source_Game_Item_h_15_EVENT_PARMS \
	struct Item_eventOnUse_Parms \
	{ \
		AFPSCharacter* Character; \
	};


#define Game_Source_Game_Item_h_15_CALLBACK_WRAPPERS
#define Game_Source_Game_Item_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUItem(); \
	friend struct Z_Construct_UClass_UItem_Statics; \
public: \
	DECLARE_CLASS(UItem, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/Game"), NO_API) \
	DECLARE_SERIALIZER(UItem)


#define Game_Source_Game_Item_h_15_INCLASS \
private: \
	static void StaticRegisterNativesUItem(); \
	friend struct Z_Construct_UClass_UItem_Statics; \
public: \
	DECLARE_CLASS(UItem, UObject, COMPILED_IN_FLAGS(CLASS_Abstract), CASTCLASS_None, TEXT("/Script/Game"), NO_API) \
	DECLARE_SERIALIZER(UItem)


#define Game_Source_Game_Item_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UItem(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UItem) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UItem); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UItem); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UItem(UItem&&); \
	NO_API UItem(const UItem&); \
public:


#define Game_Source_Game_Item_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UItem(UItem&&); \
	NO_API UItem(const UItem&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UItem); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UItem); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UItem)


#define Game_Source_Game_Item_h_15_PRIVATE_PROPERTY_OFFSET
#define Game_Source_Game_Item_h_12_PROLOG \
	Game_Source_Game_Item_h_15_EVENT_PARMS


#define Game_Source_Game_Item_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Game_Source_Game_Item_h_15_PRIVATE_PROPERTY_OFFSET \
	Game_Source_Game_Item_h_15_SPARSE_DATA \
	Game_Source_Game_Item_h_15_RPC_WRAPPERS \
	Game_Source_Game_Item_h_15_CALLBACK_WRAPPERS \
	Game_Source_Game_Item_h_15_INCLASS \
	Game_Source_Game_Item_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Game_Source_Game_Item_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Game_Source_Game_Item_h_15_PRIVATE_PROPERTY_OFFSET \
	Game_Source_Game_Item_h_15_SPARSE_DATA \
	Game_Source_Game_Item_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Game_Source_Game_Item_h_15_CALLBACK_WRAPPERS \
	Game_Source_Game_Item_h_15_INCLASS_NO_PURE_DECLS \
	Game_Source_Game_Item_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAME_API UClass* StaticClass<class UItem>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Game_Source_Game_Item_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

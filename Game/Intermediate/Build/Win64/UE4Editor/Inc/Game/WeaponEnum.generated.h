// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAME_WeaponEnum_generated_h
#error "WeaponEnum.generated.h already included, missing '#pragma once' in WeaponEnum.h"
#endif
#define GAME_WeaponEnum_generated_h

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Game_Source_Game_WeaponEnum_h


#define FOREACH_ENUM_EQUIPPEDWEAPON(op) \
	op(NullWeapon) \
	op(PrimaryWeapon) \
	op(SecondaryWeapon) \
	op(TertiaryWeapon) 
PRAGMA_ENABLE_DEPRECATION_WARNINGS

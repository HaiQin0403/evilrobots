// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef GAME_Healing_generated_h
#error "Healing.generated.h already included, missing '#pragma once' in Healing.h"
#endif
#define GAME_Healing_generated_h

#define Game_Source_Game_Healing_h_16_SPARSE_DATA
#define Game_Source_Game_Healing_h_16_RPC_WRAPPERS
#define Game_Source_Game_Healing_h_16_RPC_WRAPPERS_NO_PURE_DECLS
#define Game_Source_Game_Healing_h_16_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesUHealing(); \
	friend struct Z_Construct_UClass_UHealing_Statics; \
public: \
	DECLARE_CLASS(UHealing, UItem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Game"), NO_API) \
	DECLARE_SERIALIZER(UHealing)


#define Game_Source_Game_Healing_h_16_INCLASS \
private: \
	static void StaticRegisterNativesUHealing(); \
	friend struct Z_Construct_UClass_UHealing_Statics; \
public: \
	DECLARE_CLASS(UHealing, UItem, COMPILED_IN_FLAGS(0), CASTCLASS_None, TEXT("/Script/Game"), NO_API) \
	DECLARE_SERIALIZER(UHealing)


#define Game_Source_Game_Healing_h_16_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHealing(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(UHealing) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHealing); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHealing); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHealing(UHealing&&); \
	NO_API UHealing(const UHealing&); \
public:


#define Game_Source_Game_Healing_h_16_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API UHealing() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API UHealing(UHealing&&); \
	NO_API UHealing(const UHealing&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, UHealing); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(UHealing); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(UHealing)


#define Game_Source_Game_Healing_h_16_PRIVATE_PROPERTY_OFFSET
#define Game_Source_Game_Healing_h_13_PROLOG
#define Game_Source_Game_Healing_h_16_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Game_Source_Game_Healing_h_16_PRIVATE_PROPERTY_OFFSET \
	Game_Source_Game_Healing_h_16_SPARSE_DATA \
	Game_Source_Game_Healing_h_16_RPC_WRAPPERS \
	Game_Source_Game_Healing_h_16_INCLASS \
	Game_Source_Game_Healing_h_16_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Game_Source_Game_Healing_h_16_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Game_Source_Game_Healing_h_16_PRIVATE_PROPERTY_OFFSET \
	Game_Source_Game_Healing_h_16_SPARSE_DATA \
	Game_Source_Game_Healing_h_16_RPC_WRAPPERS_NO_PURE_DECLS \
	Game_Source_Game_Healing_h_16_INCLASS_NO_PURE_DECLS \
	Game_Source_Game_Healing_h_16_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAME_API UClass* StaticClass<class UHealing>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Game_Source_Game_Healing_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS

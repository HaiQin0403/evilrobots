// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UItem;
#ifdef GAME_FPSCharacter_generated_h
#error "FPSCharacter.generated.h already included, missing '#pragma once' in FPSCharacter.h"
#endif
#define GAME_FPSCharacter_generated_h

#define Game_Source_Game_FPSCharacter_h_20_SPARSE_DATA
#define Game_Source_Game_FPSCharacter_h_20_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUseItem); \
	DECLARE_FUNCTION(execRefreshWeapon); \
	DECLARE_FUNCTION(execRefreshEquipped); \
	DECLARE_FUNCTION(execOnTakeDamage); \
	DECLARE_FUNCTION(execStopWalking); \
	DECLARE_FUNCTION(execWalk); \
	DECLARE_FUNCTION(execShootWeapon);


#define Game_Source_Game_FPSCharacter_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUseItem); \
	DECLARE_FUNCTION(execRefreshWeapon); \
	DECLARE_FUNCTION(execRefreshEquipped); \
	DECLARE_FUNCTION(execOnTakeDamage); \
	DECLARE_FUNCTION(execStopWalking); \
	DECLARE_FUNCTION(execWalk); \
	DECLARE_FUNCTION(execShootWeapon);


#define Game_Source_Game_FPSCharacter_h_20_EVENT_PARMS \
	struct FPSCharacter_eventTakeDamage_Parms \
	{ \
		float DamageTaken; \
	};


#define Game_Source_Game_FPSCharacter_h_20_CALLBACK_WRAPPERS
#define Game_Source_Game_FPSCharacter_h_20_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFPSCharacter(); \
	friend struct Z_Construct_UClass_AFPSCharacter_Statics; \
public: \
	DECLARE_CLASS(AFPSCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Game"), NO_API) \
	DECLARE_SERIALIZER(AFPSCharacter)


#define Game_Source_Game_FPSCharacter_h_20_INCLASS \
private: \
	static void StaticRegisterNativesAFPSCharacter(); \
	friend struct Z_Construct_UClass_AFPSCharacter_Statics; \
public: \
	DECLARE_CLASS(AFPSCharacter, ACharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/Game"), NO_API) \
	DECLARE_SERIALIZER(AFPSCharacter)


#define Game_Source_Game_FPSCharacter_h_20_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFPSCharacter(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFPSCharacter) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFPSCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFPSCharacter); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFPSCharacter(AFPSCharacter&&); \
	NO_API AFPSCharacter(const AFPSCharacter&); \
public:


#define Game_Source_Game_FPSCharacter_h_20_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFPSCharacter(AFPSCharacter&&); \
	NO_API AFPSCharacter(const AFPSCharacter&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFPSCharacter); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFPSCharacter); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFPSCharacter)


#define Game_Source_Game_FPSCharacter_h_20_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__WalkAcceleration() { return STRUCT_OFFSET(AFPSCharacter, WalkAcceleration); } \
	FORCEINLINE static uint32 __PPO__SprintAcceleration() { return STRUCT_OFFSET(AFPSCharacter, SprintAcceleration); } \
	FORCEINLINE static uint32 __PPO__BaseAcceleration() { return STRUCT_OFFSET(AFPSCharacter, BaseAcceleration); } \
	FORCEINLINE static uint32 __PPO__PlayerCamera() { return STRUCT_OFFSET(AFPSCharacter, PlayerCamera); } \
	FORCEINLINE static uint32 __PPO__TurnMotion() { return STRUCT_OFFSET(AFPSCharacter, TurnMotion); } \
	FORCEINLINE static uint32 __PPO__InventoryOpened() { return STRUCT_OFFSET(AFPSCharacter, InventoryOpened); } \
	FORCEINLINE static uint32 __PPO__PlayerInventory() { return STRUCT_OFFSET(AFPSCharacter, PlayerInventory); }


#define Game_Source_Game_FPSCharacter_h_17_PROLOG \
	Game_Source_Game_FPSCharacter_h_20_EVENT_PARMS


#define Game_Source_Game_FPSCharacter_h_20_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Game_Source_Game_FPSCharacter_h_20_PRIVATE_PROPERTY_OFFSET \
	Game_Source_Game_FPSCharacter_h_20_SPARSE_DATA \
	Game_Source_Game_FPSCharacter_h_20_RPC_WRAPPERS \
	Game_Source_Game_FPSCharacter_h_20_CALLBACK_WRAPPERS \
	Game_Source_Game_FPSCharacter_h_20_INCLASS \
	Game_Source_Game_FPSCharacter_h_20_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Game_Source_Game_FPSCharacter_h_20_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Game_Source_Game_FPSCharacter_h_20_PRIVATE_PROPERTY_OFFSET \
	Game_Source_Game_FPSCharacter_h_20_SPARSE_DATA \
	Game_Source_Game_FPSCharacter_h_20_RPC_WRAPPERS_NO_PURE_DECLS \
	Game_Source_Game_FPSCharacter_h_20_CALLBACK_WRAPPERS \
	Game_Source_Game_FPSCharacter_h_20_INCLASS_NO_PURE_DECLS \
	Game_Source_Game_FPSCharacter_h_20_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> GAME_API UClass* StaticClass<class AFPSCharacter>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Game_Source_Game_FPSCharacter_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
